Imported from the Mercurial (hg) repository of the same name.

This repository contains the programs used on Campbell Scientific dataloggers at weather stations on the
H.J. Andrews Experimental Forest. The CR Basic code for each sensor, or sensor suite (e.g. 4 soil temp thermistors),
is stored in a file in the directory ./crBasic. The configuration of sensors on each logger, including port numbers
used and wire colors, is defined in met_config.yaml. When the Python program cr_assemple is run, it combines the
sensors listed in met_config.yaml into a program to be deployed on each logger.

Deployed programs are marked by V##r##a-z. V is for version number, marking major changes. r marks revision number for
minor changes. Both are set in ./src/cr_assemble.py. Any changes that only impact a single logger, such as changing port
numbers or adding/removing a sensor, are marked with a letter set in met_config.yaml. Deployed versions can be accessed
by referencing their git tag, e.g. ALLMET_V00r01.

Branches
==========
This repository has 3 branches:

#. ALLMET- this is the master branch containing working versions that are deployed at weather stations.
#. development - this branch is used to develop the program
#. met_config - this branch solely deals with changes to individual logger configurations

met_config branch
------------------
This branch is for changes to individual loggers. It does not change the methods of any sensors or how they are
programmed, it simply changes what data loggers they are installed on or which ports they are wired to. Examples
include:

* If a logger upgrades from a Kipp and Zonen CM6B pyranometer to an Apogee CS320;
* changing what port a sensor is wired to;
* changing a coefficient; or
* adding a sensor to a logger.

These changes should be limited to the met_config.yaml file. Each change on this branch
is tied to a deployed program and should have a different `wire_vers` letter for the affected logger in met_config.yaml

Legacy branches
----------------
To view legacy programs, select by tag <site name>V<num>. The original structure of the repository used a separate
branch for each site, tracking their independent CrBasic programs. These legacy branches can still be accessed to view
older versions.

As sensors were adapted into the different configuration of each site, it often lead to divergent methods and
processing between sites for a single sensor, and even different scan rates for the same sensor. This was a primary
motivation for creating the unified program.

Understanding sensor programs
==============================
A basic example sensor code is given in ./crBasic/template.cr1. Each sensor is broken into 9 sections. Each section
*must* be divided by one of `'#%^`, but every sensor does not need to have code in every section. The template
recommends a number of indents to make the code look clean and easy to read when combined into the final logger program.
 Any code added to a section will be added to that section whenever that sensor is included in a logger's met_config.
The sections are:

#. Header - this is used at the program level, but rarely added to by individual sensors
#. Declare variables - this is where variables are initiated before the program. Example:: `Public new_sensor_measurement as Float`
#. Declare SYS variables - The sys section is used for variables that are not present in the data but are only used for system diagnostics
#. SYS table - Include any system diagnostic variables in this storage table. Coefficients will be automatically added by ./src/cr_assemble.py
#. Table105 - any measurements to be stored in the final data table
#. Faster Scan - any measurement or calculation to be preformed by the program in a 1 sec scan
#. Standard Scan - any measurement or calculation to be preformed by the program in a 15 sec scan
#. Slower Scan -  any measurement or calculation to be preformed by the program at the interval of the storage table (5 min)
#. Footer - this is used at the program level, but rarely added to by individual sensors

./crBasic/met.cr1
------------------
This file initiates all tables, including the constant table in the header, as well as defining all program scans.
Because the resulting programs can have any combination of the 3 scans, there are a number of sections that are
commented, and will be uncommented automatically by the python program as needed when a logger's program is assembled.
These sections are noted in the internal comments and should be left alone unless you have an intimate knowledge of
this repository.

Contributing
=============
All HJA developers have write access to the development branch. ALLMET is by pull request only. Others can submit pull
requests for any branch. Since this project is directly linked to units deployed in the field that are turning heaters,
telemetry radios, and propane powered fluid pumps on and off all winter, expect any submission to be given a lot of
scrutiny before it's accepted.

