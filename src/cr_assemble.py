__authors__ = 'Greg Cohn'
__version__ = '0'
__revision__ = '09'

import os.path
import re
from datetime import datetime
from shutil import make_archive, move
from os import path, pardir, listdir, makedirs
import yaml


class BuildProg:
    """

    """
    def __init__(self, crpath='../crBasic/', met_config='../met_config.yaml', radio_config="../radio_config.yaml"):
        """

        """
        self.crBasic = []
        self.met_config = {}
        self.radio_config = {}

        self.load_crBasic(crpath)
        self.load_met_config(met_config)
        self.load_radio_config(radio_config)

    @staticmethod
    def _component_sort(this_part):
        """
        Rank part by matching to a list. Parts not on list will be ranked last.

        Rank sets order of 'Public' declarations which sets order of variables on keypad.

        Sensors with pauses or digital sensors (SDI/Com) must be last in their scan to avoid delays in analog
        measurements.
        :return:
        """

        order_of_parts = ['twr_air_temp', 'soil', 'precip_TipBkt', 'shlt', 'snow', 'PAR', 'net radiometer',
                          'pyranometer', 'SA', 'wind',  'PWR_shunt', 'PWR']
        rank = order_of_parts.__len__() + 1

        for i, part in enumerate(order_of_parts):
            if part in this_part:
                rank = i

        return rank

    def load_crBasic(self, crpath):
        """
        Load all crBasic files into a dictionary, keyed by file name. Then sort crBasic code into program parts:

            * header
            * public declared variables
            * SYStem function/inspection declared variables
            * SYStem function table output
            * table 105 output
            * program
            * footer

        Each crBasic file must have each component and separate them (see _find_prog_split):

            * 0 or 1 new lines (\n)
            * 1 or more line comments (multiple languages: `,#,%,*,\, or --)
            * 2 or more ^
            * 1 or more words (spaces or special characters accepted)
            * 1 or more ^
            * new line (\n)

        ..Example::
            `#%^^^^^^^^^^^^^^^^^Begin Table 105: Indent2^^^^^^^^^^^^^^^^^

        :param crpath:
        :return:
        """
        file_dict = self._read_crBasic(crpath)
        prog_dict = self._sort_crBasic_dict(file_dict)

        self.crBasic = prog_dict

    def _read_crBasic(self, f_path):
        """
        Read in all .CR# files. Each file is read in as a single string. Each files is read into a dictionary using the
        file name as dictionary keys.

        ..Example::
            SA.cr1 and snow.cr1

            are read into

            {{SA:[]}, {snow:[]}}
        :param f_path: a directory name
        :return: dictionary where key is file path and value is single string of file content
        """

        f_names = listdir(f_path)
        files = {}

        for cr in f_names:
            if not '.cr' in cr.lower(): continue
            with open(path.join(f_path, cr), 'r') as f:
                cr_no_ext = cr.split('.')[0]
                files[cr_no_ext] = f.read()

        return files

    def _sort_crBasic_dict(self, file_dict):
        """
        Loops through dictionary files loaded as single strings (see: _read_crBasic). Splits strings into a list of
        rows, and sorts into dictionary by program parts:

        :param file_dict: a dictionary of files
        :return: dictionary of program components
        """

        programs = {'header': {},
                    'public': {'SYS': {}},
                    'tbl': {'SYS': {}, '105': {}},
                    'prog': {'scan1':{}, 'scan2':{}, 'scan3':{}},
                    'footer': {}}

        # programs must follow a strict order and must have all sections, even if they are empty
        prog_order = ['header', 'public', {'public': 'SYS'}, {'tbl': 'SYS'}, {'tbl': '105'}, {'prog': 'scan1'},
                      {'prog': 'scan2'}, {'prog': 'scan3'}, 'footer']
        n_parts = len(prog_order)

        # update iteritems to items for Python v3.xx
        for file_name, prog in file_dict.items():
            split_loc = self._find_prog_split(prog)

            if len(split_loc)/2 < n_parts:
                msg = f"""{file_name}.cr1 does not have all required program parts.
                Each program must have all sections, even if they are empty."""
                raise Warning(msg)

            loc = 1
            for part in prog_order:
                start = split_loc[loc]
                end = split_loc[loc+1]
                loc += 2
                if start == end:
                    continue

                prog_lines = prog[start:end].split('\n')
                if type(part) is dict:
                        key, sub_key = list(part.items())[0]
                        programs[key][sub_key].update({file_name: prog_lines})
                elif type(part) is str:
                    programs[part].update({file_name: prog_lines})

        return programs

    def _find_prog_split(self, prog_str):
        """
        Index the split between program sections of a CRBasic program.

        ..Example::
            pyranometer.cr1


            `#%^^^^^^^^^^^^^^^^^Begin Declare SYStem Function Variables: Indent 0^^^^^^^^^^^^^^^^^
            Dim SOLAR_Wm2_RAW

            `#%^^^^^^^^^^^^^^^^^Begin SYStem Function table: Indent 2^^^^^^^^^^^^^^^^^
              Minimum(1, SOLAR_Wm2_RAW,FP2,False,0)
              Average (1,SOLAR_Wm2,FP2,0)
              Maximum (1,SOLAR_Wm2,FP2,False,0)

            `#%^^^^^^^^^^^^^^^^^Begin Table 105: Indent2^^^^^^^^^^^^^^^^^

            _find_prog_split returns: [0, 61, 61, 161, 332, 420, 438, 514, 620, 683, 749, 811]

        :param prog_str:
        :return: list of indexes of start and end of each break
        """
        breaks_iter = re.finditer("\n?['#%*\\--]{1,10}?\^{2,10}?.+\^+\n?", prog_str,
                                  flags=(re.IGNORECASE & re.MULTILINE))

        breaks = []
        for b in breaks_iter:
            breaks += b.span()

        return breaks + [prog_str.__len__()]

    def load_met_config(self, file_n='./met_config.yaml'):
        """
        Read YAML file met_config.yaml. This file contains basic information about each logger, it's sensors, and it's
        wiring.
        :param file_n: string containing filename or path
        :return: dictionary assigned to class object
        """

        with open(file_n) as f:
            lid_list = yaml.safe_load(f)

        self.met_config = lid_list

    def _convert_keys_to_float(self, dict_name):
        """
        .. Warning::
            legacy function for parsing JSON
        Converts dictionary primary (or first level) keys from string to float
        :param dict_name: dictionary
        :return: dictionary
        """
        new_dict = {}
        for k, v in dict_name.items():
            new_dict[int(k)] = v

        return new_dict

    def load_radio_config(self, yaml_path= "./radio_config.yaml"):
        """
        Load yaml containing radio transmission schedule and battery voltage thresholds for power control to instance
        object.

        :param yaml_name:
        """
        with open(yaml_path) as f:
            radio_config = yaml.safe_load(f)

        self.radio_config = radio_config

    def get_sensor_wiring(self, lid):
        """
        Get dictionary of wiring for each sensor. Variable name that stores measurements are the key, and the port
        numbers and wiring are the values.

        ..Example::
            CRBasic code to measure a temp107 thermistor:
            `Therm107 (<VarName>,1,<SEn>,<Vxn>,0,_60Hz,1.0,0)`

            Temp107 wired to Vx2 and SE12 with readings stored as AIR_TEMP:
            {'AIR_TEMP': {u'EarthGround': [u'Clear', u'Purple'],
                     u'Instrument': u'T107',
                     u'SE': {u'12': u'Red'},
                     u'Vx': {u'2': u'Black'},
                     u'calibration dates': []}}

            results in:
            ```
            Public AIR_TEMP
            Const se_AIR_TEMP = 12
            Const vx_AIR_TEMP = 2
            ...
            Therm107 (AIR_TEMP,1,se_AIR_TEMP,vx_AIR_TEMP,0,_60Hz,1.0,0)
            ```
        :param lid: int. Logger ID number
        :return: dictionary of sensors and their wiring
        """
        # python >3.6 insertion order is maintained.
        # https://mail.python.org/pipermail/python-dev/2017-December/151283.html
        components = dict(sorted(self.met_config[lid]['measure'].items(), key=self._component_sort))

        sensors = {}
        for comp in components.values():
            for varname, port_num in comp.items():
                sensors[varname] = port_num

        return sensors

    def get_prog_file_name(self, lid):
        """
        Get a filename for this lid program including correct extension.

        :param rs_num:
        :return: string of file name.
        """
        site = self.met_config[lid]['Site']
        model = self.met_config[lid]['model']
        wire_vers = self.met_config[lid]['wire_vers']

        vers = __version__
        vers = f'0{vers}' if vers.__len__() < 2 else vers
        vers += f'r{__revision__}'
        vers += wire_vers

        # pick file extension .cr1 for CR1000, .cr3 for CR3000
        if model == 1000:
            f_ext = ".cr1"
        elif model == 3000:
            f_ext = ".cr3"
        else:
            raise Warning(f'{site} {lid} has an unknown logger model with no matching file extension')

        return f'{site}_{lid}_v{vers}{f_ext}'

    def get_crd_file_prefix(self, lid):
        """
        Return a filename to prefix each file written with the CRD instruction.

        :param rs_num:
        :return: str of filename.
        """
        site = self.met_config[lid]['Site']

        return f'{site}_{lid}_'

    def get_header(self, fname):
        """
        Return a header identifying program version and time of execution.

        :param fname:
        :return:
        """
        date = datetime.now().strftime('%m-%d-%y %H:%M')
        header = [f"' Program name: {fname}",
                  f"' Auto generated by cr_assemble.py v{__version__}r{__revision__} on {date}"]

        return header

    def save_prog(self, txt, fname=None, dir_n='./bin/'):
        """
        Save assembled soure code to a text file.

        :param txt:
        :param fname:
        :param dir_n:
        """
        if fname is None:
            fname = self.get_prog_file_name()

        makedirs(dir_n, exist_ok=True)

        with open(dir_n + fname, 'w+') as f:
            f.write(txt)

    @staticmethod
    def format_aliases(aliases):
        '''
        Assigns aliases specific to this LID.

        ..Example::
            {"302":
                {"unique_prog":
                    {"aliases":{"SA_PRECIP":"PRECIP"}}}}

            Becomes:
            Alias SA_PRECIP = PRECIP

        :param sens_param: dictionary of different aliases
        :return: alias variable crBasic program
        '''

        alias_cr = []

        for s, p in aliases.items():
            alias_cr.append(f'Alias {s} = {p}')

        if alias_cr:
            alias_cr[0] = f'\n{alias_cr[0]}'
            alias_cr[-1] += '\n'

        return alias_cr

    @staticmethod
    def format_sensor_coef_off(sensor_wiring):
        '''
        cycles through logger sensors and returns strings declaring coef and offset

        :param sens_param: dictionary of different sensors.
        :return: returns sensor variables crbasic program.
        '''

        decl_sensors = []
        toSYStbl = []

        for s, p in sensor_wiring.items():
            if 'offset' in p:
                decl_sensors.append(f"Public offset_{s} = {p['offset']}")
                toSYStbl.append(f'  Sample(1,offset_{s},FP2)')
            if 'coef' in p:
                decl_sensors.append(f"Public coef_{s} = {p['coef']}")
                toSYStbl.append(f'  Sample(1,coef_{s},FP2)')

        return decl_sensors, toSYStbl

    @staticmethod
    def format_sensor_ports(sensor_wiring):
        '''
        cycles through logger sensors and returns strings declaring port numbers and voltage ranges

        :param sens_param: dictionary of different sensors.
        :return: returns sensor variables crbasic program.
        '''

        ports = []
        for s, p in sensor_wiring.items():
            if 'DiffChan' in p:
                ports.append(f"  Const diff_{s} = {list(p['DiffChan'].keys())[0]}")
            elif 'SE' in p:
                ports.append(f"  Const se_{s} = {list(p['SE'].keys())[0]}")
            elif 'PChan' in p:
                ports.append(f"  Const pchan_{s} = {list(p['PChan'].keys())[0]}")

            # voltage range determines measurement precision. Options vary by logger model.
            if "DiffVoltRange" in p:
                ports.append(f'  Const mv_{s} = {p["DiffVoltRange"]}')
            if "VxVoltRange" in p:
                # for BrHalf(), define excitation voltage
                ports.append(f"  Const vx_mv_{s} = {p['VxVoltRange']}")

            if 'CtrlPort' in p:
                ports.append(f"  Const ctrl_{s} = {list(p['CtrlPort'].keys())[0]}")
                if 'SDI_address' in p:
                    if list(p['CtrlPort'].keys())[0] not in [1, 5, 3, 7]:
                        raise ValueError('SDI sensors can only be read on Tx ports '
                                         f'(odd numbered)\nChange {p["Instrument"]} to odd C port')
                    ports.append(f'  Const sdi_{s} = "{p["SDI_address"]}"')

            if 'Com' in p:
                ports.append(f"  Const com_{s} = {list(p['Com'].keys())[0]}")

            if 'Vx' in p:
                ports.append(f"  Const vx_{s} = {list(p['Vx'].keys())[0]}")
            elif 'Ix' in p:
                ports.append(f"  Const ix_{s} = {list(p['Ix'].keys())[0]}")

        return ports

    @staticmethod
    def _comp_loop(comp, cr_section, exclude_section=''):

        section = []
        for c in comp:
            # 'snow_depth' or 'snow_water_druck' both become 'snow'
            # 'pyranometer_ModelName' becomes 'pyranometer'
            c_base = c.split('_')[0].lower()
            if c_base in exclude_section or c not in cr_section:
                continue
            section.extend(cr_section[c])

        return section

    def format_pblc_var(self, components, sensors):
        """
        Assemble all public variable declarations for the components on this logger and their sensors.

        :param components: an ordered list of components on this logger. Each component corresponds to programs in
                            ./crBasic/<names>.cr1 which can each have multiple sensors.
        :param sensors: A dictionary containing individual sensor variables and their ports
        :return: list of strings that form the Public declarations program section.
        """
        cr = self.crBasic
        # initiate empty variables
        pblc = []

        # program header, comments, and CONST table start
        pblc.extend(cr['header']['met'])
        # Const table end
        pblc.extend(self.format_sensor_ports(sensors))
        # universal Pulic variables: e.g. LID, BatteryVoltage
        pblc.extend(cr['public']['met'])

        # loop through components. Use a list with a preset order.
        pblc_comp = self._comp_loop(components, cr['public'])
        # Declare program variables, aliases, and assign units to variables from each sensor group on logger
        pblc.extend(pblc_comp)

        return pblc

    def format_power_controls(self, components, radio_settings):
        '''
        Insert the optional declarations and power table if this logger is configured for PWR and/or PWR_shunt. If PWR
        is present, this method will insert custom transmission times and battery thresholds from the radio_config.yaml
        into the program as defaults.

        Main program is automatically added by meth::self.format_prog.

        .. Note::
            Transmission times and battery thresholds are Public variables that can be changed by the operator whenever
            connected. Current values are usually preserved on reboot and may not match defaults.  However, when the
            program is resent, values will revert to defaults as defined here.

        :param components: list of components on this logger
        :param radio_settings: dict. from radio_config.yaml. Custom conection times and battery levels for this logger.
        :return: list of strings with
        '''
        cr = self.crBasic
        pwr = []

        # some stations do not have power controls for ethernet ports or radios
        # and some have controls, but no shunt to measure
        # make a list of components that don't control a radio relay, ethernet port, or measure a shunt
        other_components = ''
        for comp in components:
            if 'PWR' not in comp:
                other_components += comp.lower()

        # loop through power component programs
        decl = self._comp_loop(components, cr['public']['SYS'], exclude_section=other_components)
        # insert custom connection times and battery thresholds for this logger.
        pwr.extend(self.insert_PwrRadio_values(decl, radio_settings))

        tbl = self._comp_loop(components, cr['tbl']['SYS'], exclude_section=other_components)
        pwr.extend(tbl)
        # end the table if it has contents
        if pwr:
            pwr.append('EndTable\n')

        return pwr

    def _format_sys_tbl(self, components, sensors, unique_prog={}):

        cr = self.crBasic
        cr_sys_tbl = cr['tbl']['SYS']
        tbl_sys = []

        # call/initiate SYS table
        tbl_sys.extend(cr_sys_tbl['met'])

        # for each component other than SA, look for values that are stored in the SYS table
        tbl_comp = self._comp_loop(components, cr_sys_tbl, exclude_section='sa pwr')
        tbl_sys.extend(tbl_comp)

        unq_tbl = self._comp_loop(['tblSYS', 'TableSYS', 'SYStbl', 'SYSTable'], unique_prog)
        tbl_sys.extend(unq_tbl)

        # place each of the sensor coefficients and offsets into the SYS table
        _, sensor_tbl = self.format_sensor_coef_off(sensors)
        tbl_sys.extend(sensor_tbl)

        # end the table
        tbl_sys.append('EndTable\n')

        return tbl_sys

    def _format_sys_decl(self, components, sensors):

        cr = self.crBasic
        cr_sys_p = cr['public']['SYS']

        sys_param = []

        # system diagnostics header
        sys_param.extend(cr_sys_p['met'])

        # for each component other than SA, look for values that are declared
        decl_comp = self._comp_loop(components, cr_sys_p, exclude_section='sa pwr')
        sys_param.extend(decl_comp)

        # Add public variables controlling wiring and ports
        # coefficients, offsets, Vx, SE, COM, Diff numbers
        sensor_decl, _ = self.format_sensor_coef_off(sensors)
        sys_param.extend(sensor_decl)
        sys_param[-1] += '\n'

        return sys_param

    def format_sys(self, components, sensors, unique_prog={}):
        """
        The SYS section was created in 2016 to create or store auxiliary variables and system diagnostics in the
        program without changing the data stream of our long term measurements. These variables are not included in
        final data or Table105 and are simply diagnostic, e.g. intermediate values in a calculation.

        System information is split into 2 sections: 1) declarations of variables; 2) storage of variables that are for
        diagnostics only in a separate SYS table.

        :param components: an ordered list of components on this logger. Each component corresponds to programs in
                            ./crBasic/<names>.cr1 which can each have multiple sensors.
        :param sensors: A dictionary containing individual sensor variables and their ports
        :param unique_prog: A dictionary whose values are a unique crBasic code keyed to the correct program section.
        :return: list of strings that form the SYS program section.
        """
        # System parameters include both declaration of special variables, and a table output that changes depending on
        # which sensors and components are present
        unq_sys = self._comp_loop(['public', 'publicSYS', 'SYSdecl', 'SYSpublic'], unique_prog)
        aliases =[unique_prog['alias'] if 'alias' in unique_prog else{}][0]

        sys = self._format_sys_decl(components, sensors) + \
              unq_sys + self.format_aliases(aliases) +\
              self._format_sys_tbl(components, sensors, unique_prog)

        return sys

    def format_tbl_105(self, components, unique_prog={}):
        """
        Assemble all variables stored in Table105 for the components on this logger and their sensors.

        :param components: an ordered list of components on this logger. Each component corresponds to programs in
                            ./crBasic/<names>.cr1 which can each have multiple sensors.
        :param sensors: A dictionary containing individual sensor variables and their ports
        :return: list of strings that form the Table105 program section.
        """
        cr = self.crBasic
        tbl_105 = []

        # start 105 tables with table call
        tbl_105.extend(cr['tbl']['105']['met'])

       # Assemble Table105 and sensor measurements from list of sensor groups on this logger
        tbl_comp = self._comp_loop(components, cr['tbl']['105'])
        tbl_105.extend(tbl_comp)

        unq_tbl = self._comp_loop(['Table105', 'tbl105'], unique_prog)
        tbl_105.extend(unq_tbl)

        # end tables
        tbl_105.append('EndTable\n')

        return tbl_105

    def format_prog(self, components, unique_prog):
        """
        Setup the program executed by the data logger. These are the main actions taken within scans.

        Method accumulates components executed within each scan. If unique programs defined in the config file
        include a scan keyword, they are also added. Scans with no components or unique programs are not included.

        Since 3 combinations of the 3 scans (fast 1 sec; main 15 sec; slow 5 min) are possible (1+15, 1+15+5, 15+5),
        `NextScan`, `CallTable`, and `SlowSequence/EndSequence` commands are commented in the :doc:`.crBasic/met.cr1`
        and are uncommented by this method based on the combination of scans for a given LID.

        :param components: an ordered list of components on this logger. Each component corresponds to programs in
                            ./crBasic/<names>.cr1 which can each have multiple sensors.
        :param unique_prog: A dictionary whose values are a unique crBasic code keyed to the correct program section.
        :return: list of strings that form the program section.
        """
        cr = self.crBasic

        # place main program header
        prog = ["'\\\\\\\\\\\\\\\\\\\\\\\\\\\\ MAIN PROGRAM ////////////////////////////",
                'BeginProg']

        scan_inventory = [False]*3
        for n, scan in enumerate(cr['prog']):
            # Get program execution for this scan from each sensor group on logger
            # write program calls
            prog_comp = self._comp_loop(components, cr['prog'][scan])
            unq_prog = self._comp_loop([scan], unique_prog)

            if prog_comp or unq_prog:
                # if this logger has sensors in this scan,
                # uncomment parts of met.cr1 to adjust to this logger's combinations of scans and/or slowsequences
                scan_inventory[n] = True
                uncomment = []
                # if the primary scan follows a faster scan add scan and sequence calls
                if scan_inventory[0] and scan_inventory[1] and not scan_inventory[2]:
                    uncomment = ['NextScan'.lower(), 'SlowSequence'.lower(), 'CallTable'.lower()]
                elif not scan_inventory[0] and scan_inventory[1] and scan_inventory[2]:
                    # if the primary scan is the fastest/first scan, CallTable before starting this scan
                    uncomment = ['CallTable'.lower()]
                elif scan_inventory[2] and scan_inventory[0]:
                    # if the primary scan (2nd) followed a faster scan, then end the primary sequence before this scan
                    uncomment = ['EndSequence'.lower()]

                # uncomment selected lines
                met = cr['prog'][scan]['met']
                for u in uncomment:
                    met = [i.replace("'", "") if u in i.lower() else i for i in met]

                prog.extend(met)
                # unique entries for this logger are added first on the presumption that they are analog
                prog.extend(unq_prog)
                prog.extend(prog_comp)

        prog_foot = self._comp_loop(components, cr['footer'])
        prog.extend(prog_foot)

        met_footer = cr['footer']['met']
        if scan_inventory.count(True) == 1:
            # if there is only one scan, CallTable is in the footer
            foot_uncomment = 'CallTable'.lower()
        elif scan_inventory[0] or scan_inventory[2]:
            # if there are >=2 scans, end the slow sequence in the footer
            foot_uncomment = 'EndSequence'.lower()

        #if footer is preceeded by a slowsequence (i.e. there is more than one scan)
        met_footer = [i.replace("'", "") if foot_uncomment in i.lower() else i for i in met_footer]

        prog.extend(met_footer)

        return prog

    @staticmethod
    def move_asp_speed2fast_scan(textstr):
        """
        Insert commands into table 105 that are unique to this lid.

        :param textstr: A multiline text string containing the entire assembled program for this logger.
        :return: A modified multiline text string containing the entire assembled program for this logger.
        """
        # find ASP_SPEED tachometer measurement
        meas_asp = re.compile('(\n.+\n.*PulseCount\(ASP.+\n)' , flags=(re.MULTILINE & re.IGNORECASE))
        # copy ASP_SPEED measurement to clipboard
        copy_asp_to_clipboard = meas_asp.findall(textstr)
        # cut string from program
        cut_asp_from_str = meas_asp.sub('', textstr)
        # paste ASP_SPEED measurement at end of fast sequence
        for paste in copy_asp_to_clipboard:
            cut_asp_from_str = re.sub('(?<=\s{2}Scan \(SCAN_INTERVAL_FAST,Sec,0,0\)\n)', paste, cut_asp_from_str,
                                      flags=(re.MULTILINE & re.IGNORECASE))

        return cut_asp_from_str

    @staticmethod
    def insert_PwrRadio_values(pwr, radio_settings):
        """
        This is the ONLY *insert* method that works on a list of strings where each string is a line. That is because all
        power settings are always added as a small group and all changes only happen in the SYS section.

        :param pwr: list of strings from the public-SYS section of PWR.cr1
        :param radio_settings: dictionary of logger specific values
        :return: a new list of strings
        """
        new_pwr = []
        # set logger specific values
        for i, line in enumerate(pwr):
            new_pblc = None
            for k, v in radio_settings.items():
                # make sure alias declarations (e.g. RADIO_INTVLS(2) = RADIO_now_INTVL) are skipped
                if k.lower() in line.lower() and 'public' in line.lower():
                    pblc, colon, unit = line.partition(':')
                    decl, equals, val = pblc.partition('=')

                    new_pblc = decl + equals + str(v)
                    spaces = len(pblc) - len(new_pblc)

                    new_pwr.append(new_pblc + ' ' * spaces + colon + unit)

            if not new_pblc:
                new_pwr.append(line)

        return new_pwr

    @classmethod
    def insert_into_tbl105(cls, textstr, add, comp):
        """
        Insert commands into table 105 that are unique to this lid.

        :param textstr: A multiline text string containing the entire assembled program for this logger.
        :param add: A dictionary of lines to be inserted
        :param comp: A list of components used to select which keys from "add" to insert
        :return: A modified multiline text string containing the entire assembled program for this logger.
        """
        # Start of Table105 and ends the line before EndTable (?=) is 'look ahead'
        match = re.search('^DataTable\(Table105.+(?=\s*EndTable$)', textstr, flags=(re.MULTILINE | re.DOTALL))
        return cls._insert_line(textstr, match, add, comp)

    @classmethod
    def insert_snow_offsets(cls, textstr, sensors):
        """
        Inserts offset values for snow sensors, defining the baseline, no-snow value or tare.

        :param textstr: A multiline text string containing the entire assembled program for this logger.
        :param sensors: list of sensors present for this lid.
        :return: altered text str.
        """

        snow = {'SNOW_TEMP_CORR_DISTANCE': 'SNOW_INITIAL_DISTANCE', 'SWE': 'SWE_TARE'}
        for s, n in snow.items():
            if s in sensors:
                no_snow_value = f' = {sensors[s][n]}'
                match = re.search('(^(Public|Dim|Const){1}\s+' + n + '{1})(?=.*(:UNITS|\n))', textstr,
                                 flags=re.IGNORECASE | re.MULTILINE)

                # clean up white space and indentations. Exp: `Public SWE_TARE   :UNITS SWE_TARE = mm` don't move :UNITS
                textstr = cls._insert_line(textstr, match, {'tare': no_snow_value}, ['tare'])

        return textstr

    @classmethod
    def insert_check_reset(cls, textstr, comp):
        """
        Inserts check values for components of this lid at the end of the program.

        :param textstr: str to be altered.
        :param comp: list of components present for this lid.
        :return: altered text str.
        """

        check = {'SA': '        PUMPVOLTS_CHECK = NAN\n',
                 'snow_depth': '        SNOWDEPTH_CHECK = NAN\n',
                 'snow_water': '        SWE_CHECK = NAN\n'}

        match = re.search('If.*\s*[^Public]BATTERY_V_CHECK.*=.*NAN\n', textstr, flags=(re.IGNORECASE))

        return cls._insert_line(textstr, match, check, comp)

    def insert_download_filename(self, lid, cr_text):
        """
    `   Insert the file name to be generated when downloaded and change TableFile options to fit the download device
        (CRD or USB).

        Example::
            ./crBasic/met.cr1
            TableFile("CRD:_Table105_", 64, -1, 0, 1, Day, 0, 0)

            Becomes-
            TableFile("CRD:prim_230_Table105_", 64, -1, 0, 1, Day, 0, 0)
            or
            TableFile("USB:prim_230_Table105_", 8, -1, 0, 0, Hr, 0, 0)

        :param lid: int of logger id.
        :param cr_text: A multiline text string containing the entire assembled program for this logger.
        :return: A modified multiline text string containing the entire assembled program for this logger.
        """
        # site_lid can be inserted with a simple sub, and CRD: could be changed to USB: the same way.
        # The problem is that TableFileOption needs to change from TOB3 to TOA5 and interval also needs to change.
        storage_dev = self.met_config[lid]['DownloadDevice']
        prefix = f'USB:{self.get_crd_file_prefix(lid)}'
        # find every instance where a TableFile is called to assign to a storage device/
        match = re.finditer('(?<=TableFile\(").*\n', cr_text, flags=(re.MULTILINE | re.IGNORECASE))

        pos_shift = 0
        for m in match:
            # split TableFile() command into it's input parameters and reassign
            tblfile_inputs = m.group().partition(':')

            insert_tbl_name = prefix + tblfile_inputs[-1]
            addcard = ''
            if storage_dev in 'crd|CRD|card|CARD|Card':
                addcard = '  CardOut (0 ,-1)\n'
                if 'CONT' in m.group():
                    addcard = addcard.replace('-1', '12000')

            # TableFile("USB:prim_230_Table105_", 8, -1, 0, 0, Hr, 0, 0)
            # TOA5

            # Adjust pointer based on the difference between the length of the inserted text and the original
            start = m.start() + pos_shift
            end = m.end() + pos_shift

            cr_text = cr_text[:start] + insert_tbl_name + addcard + cr_text[end:]

            pos_shift += len(insert_tbl_name) - len(m.group()) + len(addcard)

        return cr_text

    @staticmethod
    def insert_nr01Htr_maxA(cr_txt, maxA):
        """

        :param cr_txt: A multiline text string containing the entire assembled program for this logger.
        :param maxA: float of maximum amp hours that nr01 htr can use in a day.
        :return: A modified multiline text string containing the entire assembled program for this logger.
        """
        return re.sub('(?<=Public\snr01Pwr\(3\) =)(?=\n)', f' {maxA}', cr_txt, flags=re.IGNORECASE&re.MULTILINE)

    @classmethod
    def insert_solar_rh(cls, cr_txt, dewpt):
        """

        :param cr_txt: A multiline text string containing the entire assembled program for this logger.
        :param dewpt: str of variable name for dewpoint calcualtion on this logger
        :return: A modified multiline text string containing the entire assembled program for this logger.
        """
        # if assign an rh/dewpoint sensor to the pyranometer heater program
        # if sensor absent, re will return text unchanged.
        if not dewpt is False:
            # CS320 heat based on difference from dewpoint
            cr_txt = re.sub("(?<=(solar_DewDif = solar_temp - )).*DewPnt", dewpt, cr_txt, flags=re.IGNORECASE)
            # nr01 assign dewpoint from other variable.
            cr_txt = re.sub("(?<=(DewPt = Table105\.)).*(?=\()", f'{dewpt}_Avg', cr_txt, flags=re.IGNORECASE)
            has_rh = 'Const has_rh As Boolean = True\n\n'
        else:
            has_rh = 'Const has_rh As Boolean = False\n\n'

        # find spot to insert has_rh
        research = re.search("(?=(DataTable\(SYS,true,-1\)))", cr_txt, flags=re.IGNORECASE)

        return cls._insert_line(cr_txt, research, {'has_rh': has_rh}, ['has_rh'])

    @staticmethod
    def _insert_line(textstr, regex_match, insert_str, inclusion_key):
        """
        Inserts additional strings into a text string at a location identified by a regex match. insert_str is a
        dictionary that is filterd by the inclusion key.
        :param textstr: str to be altered
        :param regex_match: meth::re.match() object identifying location
        :param insert_str: dict of strings to be inserted
        :param inclusion_key: list of keys to be inserted from insert_str.
        :return: altered str.
        """
        if not regex_match:
            raise ValueError('Regex could not find position in text')

        i = regex_match.end()
        for inst in insert_str:
            if [ik for ik in inclusion_key if inst.lower() in ik.lower()]:
                textstr = textstr[:i] + insert_str[inst] + textstr[i:]
                i += insert_str[inst].__len__()

        return textstr

    def is_prog_component(self, component):
        """
        Test if a met station component has a corresponding program component. This is a proxy for
        `exist(<ComponentName.cr1>`.

        This does not check for a file in directory, but instead checks .cr1 files loaded into the dictionary
        `self.crBasic` in an instance of this class.

        This method is designed to check components listed in the met station configuration file, `met.config`, but can
        be used in a generalized way.

        ..Note::
            Future concerns, this compares to self.crBasic['prog']
            'prog' key may not work in all future situations

        :param component: str. A name of a met station component.
        :return: Boolean.
        """
        public = self.crBasic['public']
        pblc_sys = self.crBasic['public']['SYS']

        return component in public or component in pblc_sys

    def build_met_prog(self, lid, fpath='../bin/'):
        """

        :param lid:
        :return:
        """
        # INITIALIZE and CHECK
        ######################################################
        cr = self.crBasic
        # sort this logger's components into desired/standard order
        components = sorted(self.met_config[lid]['measure'], key=self._component_sort)
        wire_vers = self.met_config[lid]['wire_vers']

        radio_settings = self.radio_config[lid] if 'PWR' in components else {}

        # check that there is a file that matches each component
        comp_has_file = [self.is_prog_component(c) for c in components]
        if not all(comp_has_file):
            raise NameError(lid, ': ', 'There is no corresponding program', components[comp_has_file.index(False)])

        # GET PROGRAM PARTS
        ######################################################
        # compile a dict of all sensors from each component
        sensors = self.get_sensor_wiring(lid)

        unq_prog = {}
        if 'unique_prog' in self.met_config[lid]:
            unq_prog = self.met_config[lid]['unique_prog']

        vers_str = f'{__version__}r{__revision__}{wire_vers}'
        vers_flt = f'{__version__}.{__revision__}{wire_vers}'

        fname = self.get_prog_file_name(lid)
        header = self.get_header(fname)

        # Assemble file: declare variables, table output, and write program
        if components.count('SA'):
            sa = cr['public']['SYS']['SA'] + cr['tbl']['SYS']['SA']
        else:
            sa = []

        # ASSEMBLE PROGRAM
        ######################################################

        cr_file = header + \
                  self.format_pblc_var(components, sensors) + \
                  sa + \
                  self.format_power_controls(components, radio_settings) + \
                  self.format_sys(components, sensors, unq_prog) + \
                  self.format_tbl_105(components, unq_prog) + \
                  self.format_prog(components, unq_prog)
        #######################

        # convert list to string
        cr_text = "\n".join(cr_file)

        # INSERT CUSTOM CHANGES
        ######################################################
        # insert check values
        cr_text = self.insert_check_reset(cr_text, components)

        # snow sensor distance above ground and/or swe tare value
        cr_text = self.insert_snow_offsets(cr_text, sensors)

        # place aspirated in fast scan if it exists (if wind sensors present)
        if [k for k in components if 'wind' in k.lower()] and \
                ('twr_air_temp' in components or 'twr_replicate_asp_temp' in components):
            cr_text = self.move_asp_speed2fast_scan(cr_text)

        # Set dwpt for pyran/nr01 heater to use
        if 'has_rh' in self.met_config[lid]:
            dewpt = self.met_config[lid]['has_rh']
            cr_text = self.insert_solar_rh(cr_text, dewpt)

        if 'net_radiometer' in components:
            cr_text = self.insert_nr01Htr_maxA(cr_text, sensors['nr01Htr']['max_ah'])

        # fill in site specific values
        # !!-----------------------------!!
        # logger ID
        cr_text = re.sub('^(CONST).*LID.*', f'Const LID                = {lid}', cr_text,
                         flags=re.IGNORECASE | re.MULTILINE)

        cr_text = re.sub('.*StationName.*',
                    f'StationName              = {self.met_config[lid]["Site"]}_{lid}',
                         cr_text, flags=re.IGNORECASE | re.MULTILINE)

        # file download name and method
        cr_text = self.insert_download_filename(lid, cr_text)

        # remove radio relay control circuit where none exists, but leave ethernet port controls
        if 'RADIO_PWR' not in sensors:
            cr_text = re.sub('(\s*PortGet\(.*ctrl_RADIO_PWR.*\)){1}.*', '', cr_text, flags=re.IGNORECASE)
            cr_text = re.sub('(PortSet\(.*ctrl_RADIO_PWR.*(on){1}.*\)){1}.*', 'RADIO_isON = on', cr_text,
                             flags=re.IGNORECASE)
            cr_text = re.sub('(PortSet\(.*ctrl_RADIO_PWR.*(off){1}.*\)){1}.*', 'RADIO_isON = off', cr_text,
                             flags=re.IGNORECASE)

        # program version
        cr_text = re.sub('(^[Public|Dim|Const]+.*PROG_VERS+.*$)',
                         f'Public PROG_VERS As String *4 = "{vers_flt}"',
                         cr_text, flags=re.IGNORECASE | re.MULTILINE)
        '''
        # A simple string replace is faster, but is more prone to errors (case, sensitive exact matches only)
        # and a list is unbearably slow. Even adding the time of converting list to string regex is faster
        %%timeit
        crf = cr_file
        for c in crf:
            if 'LID' in c:
                edit = c.replace('=', '= 234')
                i = crf.index(c)
                crf.remove(c)
                crf.insert(i, edit)
        
        100 loops, best of 3: 6.64 ms per loop  
        
        %%timeit  
        cr_text = '/n'.join(cr_file)
        
        10000 loops, best of 3: 54.6 us per loop
        
        # both replacement methods for a string are faster than iterating over the list        
        %%timeit 
        re.sub('^[CONST|PUBLIC|DIM].*LID.*$','Const LID = 234', cr_text, re.MULTILINE)
        
        10000 loops, best of 3: 104 us per loop
        
        %%timeit
         cr_text.replace('LID =', 'LID = 234')

        100000 loops, best of 3: 7.46 us per loop
        '''

        self.save_prog(cr_text, fname, dir_n=fpath)
        print(f'Successful write to {fpath}{fname}\n')

    def build_all_prog(self, fpath='../bin/'):
        """

        :return:
        """
        lid_list = self.met_config

        for lid in lid_list.keys():
            self.build_met_prog(lid, fpath)

        print(f'-------------------------------------\nAll stations sucessfully written to {fpath}')

    def zip_all_prog(self, outpath='bin'):
        """

        :return:
        """
        fpath = path.abspath(pardir)
        name = f'MET_v{__version__}r{__revision__}'
        zippath = f'{path.join(fpath, outpath, name)}.zip'
        if os.path.exists(zippath):
            os.remove(zippath)

        make_archive(name, 'zip', fpath, 'bin')
        move(f'{name}.zip', path.join(fpath, outpath))

if __name__ == "__main__":
    programs = BuildProg()
    #t = programs.build_met_prog(234)
    # programs
    # with open('../bin/modules_TestCent.cr1', 'w') as f:
    #     f.write(t)#"\n".join(t))
    programs.build_all_prog()
    programs.zip_all_prog()
